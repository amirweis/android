package com.weissamir.guessnumber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    int myNumber;

    int correctCount = 0;

    public int getMyNumber() {
        return myNumber;
    }

    public void setMyNumber(int myNumber){
        this.myNumber = myNumber;
    }

    public void setMyRandomNumber(int size) {
        Random rand = new Random();

// Obtain a number between [0 - 19].
        int n = rand.nextInt(size);

// Add 1 to the result to get a number from the required range
// (i.e., [1 - 20]).
        n += 1;
        setMyNumber(n);
        Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
    }

    public int getCorrectCount() {
        return correctCount;
    }

    public void setCorrectCount(int correctCount) {
        this.correctCount = correctCount;
    }

    private void increaseCorrectCount(){
        setCorrectCount(getCorrectCount()+1);
    }

    public void makeToast(String str){
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }
    public void guessClicked(View view){
        EditText guessAmount = (EditText) findViewById(R.id.guessText);
        Log.i("Guess", guessAmount.getText().toString());
        int intGuess = Integer.parseInt(guessAmount.getText().toString());
        if (intGuess == 0){
            makeToast("You guess correct " + Integer.toString(getCorrectCount()) + " numbers");
            Log.i("Correct Guesses", Integer.toString(getCorrectCount()));
            System.exit(0);
        } else if(intGuess > getMyNumber()) {
            makeToast("Lower");
        } else if (intGuess < getMyNumber()) {
            makeToast("Higher");
        } else {
            makeToast("Correct");
            Log.i("Guess", "Correct");
            increaseCorrectCount();
            setMyRandomNumber(20);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setMyRandomNumber(20);

    }
}
