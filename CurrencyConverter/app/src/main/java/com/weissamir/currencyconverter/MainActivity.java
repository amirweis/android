package com.weissamir.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import static java.lang.Number.*;

public class MainActivity extends AppCompatActivity {

    public void convertButtonClicked(View view){

        EditText amount = (EditText) findViewById(R.id.convertAmount);
        Double result = Double.parseDouble(amount.getText().toString()) * 3.7;
        Toast.makeText(this, "NIS " + String.format("%.2f",result), Toast.LENGTH_SHORT).show();
        Log.i("Clicked", "convertButton");

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
