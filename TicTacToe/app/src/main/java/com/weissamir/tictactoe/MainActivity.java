package com.weissamir.tictactoe;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    // red = 1; yellow = 0
    int activePlayer = 1;
    String[] player = {"Yellow", "Red"};
    int[] gameState =  {-1, -1, -1, -1, -1, -1, -1, -1, -1};
    int[][] winningPositions = {{0,1,2},{3,4,5},{6,7,8},{0,3,6},{1,4,7},{2,5,8},{0,4,8},{2,4,6}};

    public void dropIn (View view){

        ImageView counter = (ImageView) view;
        int tappedCounter = Integer.parseInt(counter.getTag().toString());
        if(gameState[tappedCounter] == -1) {
            gameState[tappedCounter] = activePlayer;

            counter.setTranslationY(-1000f);

            counter.setImageResource(activePlayer == 1 ? R.drawable.red : R.drawable.yellow);
            /*
            if (activePlayer == 1) {
                counter.setImageResource(R.drawable.red);
            } else {
                counter.setImageResource(R.drawable.yellow);
            }
            */
            counter.animate().translationYBy(1000f).setDuration(300);
        }

        if (winner(activePlayer)){
            Toast.makeText(this, player[activePlayer] + " is the winner!!!", Toast.LENGTH_LONG).show();
            resetBoard();
        }else if(fullBoard()){
            Toast.makeText(this, "It is a tie!", Toast.LENGTH_LONG).show();
            resetBoard();
        }

        activePlayer = activePlayer == 1 ? 0 : 1;
    }

    private boolean fullBoard(){
        boolean response = true;
        for(int i=0;i<9;i++){
            if(gameState[i] == -1){
                response = false;
                break;
            }
        }
        return response;
    }

    private void resetBoard(){

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    private boolean winner(int activePlayer){
        boolean response = false;
        for(int[] winningPosition : winningPositions){
           if(gameState[winningPosition[0]] == activePlayer &&
                   gameState[winningPosition[0]] == gameState[winningPosition[1]] &&
                   gameState[winningPosition[1]] == gameState[winningPosition[2]]) {
               response = true;
           }
        }
        return response;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
