package com.robpercival.demoapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public void logInClicked(View view) {

        ImageView cimg;
        EditText myUsername = (EditText) findViewById(R.id.myUsername);
        EditText myPassword = (EditText) findViewById(R.id.myPassword);
        cimg = (ImageView) findViewById(R.id.imageView);

        Toast.makeText(MainActivity.this, "Hi " + myUsername.getText().toString() + "!", Toast.LENGTH_LONG).show();
        cimg.setImageResource(R.drawable.car2);

        Log.i("Username", myUsername.getText().toString());
        Log.i("Password", myPassword.getText().toString());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
