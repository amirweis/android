package com.weissamir.layouts;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    public void fade(View view){

        ImageView bartImage = (ImageView) findViewById(R.id.bartImage);
        //ImageView homerImage = (ImageView) findViewById(R.id.homerImage);
        //Log.i("Bart  Alpha", String.valueOf(bartImage.getAlpha()));
        //Log.i("Homer Alpha", String.valueOf(homerImage.getAlpha()));

        // Fade
        /*if(bartImage.getAlpha() > homerImage.getAlpha()) {
            bartImage.animate().alpha(0f).setDuration(2000);
            homerImage.animate().alpha(1f).setDuration(2000);
        } else {
            bartImage.animate().alpha(1f).setDuration(2000);
            homerImage.animate().alpha(0f).setDuration(2000);
        }*/

        // move to the left
        //bartImage.animate().translationXBy(-1000f).setDuration(2000);

        // move done
        //bartImage.animate().translationYBy(1000f).setDuration(2000);

        // rotate clockwise
        //bartImage.animate().rotationBy(180f).setDuration(2000);

        // shrink size
        //bartImage.animate().scaleX(0.5f).scaleY(0.5f).setDuration(2000);

        // move right, left, rotate 360
        bartImage.animate().translationXBy(100f).translationYBy(-100f).scaleX(0.5f).scaleY(0.5f).rotationBy(3600f).setDuration(2000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
