package com.weissamir.nunbershapes;

/**
 * Created by AMIRW on 3/7/2019.
 */

class Number {

    int number;

    public boolean isSquare(){

        Double squareRoot = Math.sqrt(number);

        return (squareRoot == Math.floor(squareRoot) ? true : false);

    }

    public boolean isTriangle(){

        int x = 1;
        int triangleNumber = 1;

        while (triangleNumber < number){

            x++;

            triangleNumber += x;

        }

        return (triangleNumber == number ? true : false);

    }


}