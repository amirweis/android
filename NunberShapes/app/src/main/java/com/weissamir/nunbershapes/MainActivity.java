package com.weissamir.nunbershapes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public void testNumber(View view){

        String message = "";

        EditText usersNumber = (EditText) findViewById(R.id.usersNumber);

        if (usersNumber.getText().toString().isEmpty()){
            message = "Please enter a number";
        } else {

            Log.i("usersNumber", usersNumber.getText().toString());

            Number myNumber = new Number();
            myNumber.number = Integer.parseInt(usersNumber.getText().toString());

            Log.i("Square",String.valueOf(myNumber.isSquare()));
            Log.i("Triangle",String.valueOf(myNumber.isTriangle()));

            if (myNumber.isSquare()) {
                if (myNumber.isTriangle()) {
                    message = myNumber.number + " is both triangular and square!";
                } else {
                    message = myNumber.number + " is only square";
                }
            } else {
                if (myNumber.isTriangle()) {
                    message = myNumber.number + " is only triangular";
                } else {
                    message = myNumber.number + " is neither triangular nor square!";
                }
            }
        }
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText edittext=(EditText)findViewById(R.id.usersNumber);
        final Button submit=(Button)findViewById(R.id.button);

        edittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER || event.getKeyCode() == KeyEvent.KEYCODE_NUMPAD_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)){
                    //do what you want on the press of 'done'
                    submit.performClick();
                }
                return false;
            }
        });

    }

}
